USE [master]
GO
/****** Object:  Database [PhoneBookDB]    Script Date: 26.10.2016 11:39:03 ******/
CREATE DATABASE [PhoneBookDB]
GO
begin
EXEC [PhoneBookDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PhoneBookDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PhoneBookDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PhoneBookDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PhoneBookDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PhoneBookDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [PhoneBookDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PhoneBookDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PhoneBookDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PhoneBookDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PhoneBookDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PhoneBookDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PhoneBookDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PhoneBookDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PhoneBookDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PhoneBookDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PhoneBookDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PhoneBookDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PhoneBookDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PhoneBookDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PhoneBookDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PhoneBookDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PhoneBookDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PhoneBookDB] SET RECOVERY FULL 
GO
ALTER DATABASE [PhoneBookDB] SET  MULTI_USER 
GO
ALTER DATABASE [PhoneBookDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PhoneBookDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PhoneBookDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PhoneBookDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PhoneBookDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PhoneBookDB', N'ON'
GO
USE [PhoneBookDB]
GO
/****** Object:  Table [dbo].[tDepartment]    Script Date: 26.10.2016 11:39:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tDepartment](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[DepName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tDepartment] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tEmployee]    Script Date: 26.10.2016 11:39:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tEmployee](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Internal] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
 CONSTRAINT [PK_tEmployee] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tDepartment] ON 

GO
INSERT [dbo].[tDepartment] ([DepartmentID], [DepName]) VALUES (1, N'Adalet Meslek Yüksek Okulu')
GO
INSERT [dbo].[tDepartment] ([DepartmentID], [DepName]) VALUES (2, N'Adalet Programı')
GO
INSERT [dbo].[tDepartment] ([DepartmentID], [DepName]) VALUES (3, N'Ayniyat Birimi')
GO
INSERT [dbo].[tDepartment] ([DepartmentID], [DepName]) VALUES (4, N'Bankacılık ve Sigortacılık Programı')
GO
INSERT [dbo].[tDepartment] ([DepartmentID], [DepName]) VALUES (5, N'Bilgisayar Mühendisliği Bölümü')
GO
INSERT [dbo].[tDepartment] ([DepartmentID], [DepName]) VALUES (6, N'Mimarlık Bölümü')
GO
SET IDENTITY_INSERT [dbo].[tDepartment] OFF
GO
SET IDENTITY_INSERT [dbo].[tEmployee] ON 

GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (1, N'Yrd.Doç.Dr.', N'Akhan', N'Akbulut', 4217, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (2, N'Muh', N'Arda', N'Arşık', 4125, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (3, N'Arş.Grv.', N'Arda', N'Uslu', 4234, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (4, N'Yrd.Doç.Dr.', N'Bahar ', N'İlgen', 4298, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (5, N'Doç.Dr.', N'Çağatay', N'Çatal', 4215, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (6, N'Arş.Grv.', N'Ezgi ', N'Demircan Türeyen', 4719, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (7, N'Muh.', N'Fatma', N'Patlar Akbulut', 4854, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (8, N'Muh.', N'Melike', N'Günay', 4214, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (9, N'Muh. ', N'Öznur', N'Şengel', 4720, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (10, N'Sekreter', N'Sibel', N'Çuhacı', 4207, 5)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (11, NULL, N'Caner', N'Taşkan', 4857, 3)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (12, NULL, N'Tuncer', N'Aydın', 4862, 3)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (13, N'Yrd.Doç.Dr.', N'Asiye', N'Akgün Gültekin', 4018, 6)
GO
INSERT [dbo].[tEmployee] ([EmployeeID], [Title], [FirstName], [LastName], [Internal], [DepartmentID]) VALUES (14, N'Arş.Grv.', N'Emine Merve', N'Aksoy', 4815, 6)
GO
SET IDENTITY_INSERT [dbo].[tEmployee] OFF
GO
ALTER TABLE [dbo].[tEmployee]  WITH CHECK ADD  CONSTRAINT [FK_tEmployee_tDepartment] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[tDepartment] ([DepartmentID])
GO
ALTER TABLE [dbo].[tEmployee] CHECK CONSTRAINT [FK_tEmployee_tDepartment]
GO
USE [master]
GO
ALTER DATABASE [PhoneBookDB] SET  READ_WRITE 
GO